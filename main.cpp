#include <iostream>

#include "ubicacio.hpp"
#include "contenidor.hpp"
#include "cataleg.hpp"
#include "terminal.hpp"

int main()
{
	terminal t(1, 2, 3, terminal::FIRST_FIT);
	contenidor c("HE", 10);
	ubicacio u(1, 2, 3);
	t.insereix_contenidor(c);
	return 0;
}
