#include "ubicacio.hpp"

ubicacio::ubicacio(int i, int j, int k) throw(error)
{
	// <-1,0,0> si el contenidor està en l'àrea d'espera
	// <-1,-1,-1> si el contenidor no està en la terminal
	if (!((i == -1 && j == 0 && k == 0) ||
	        (i == -1 && j == -1 && k == -1) ||
	        (i > 0 && j > 0 && k > 0)))
		throw error(UbicacioIncorrecta);
	c_i = i;
	c_j = j;
	c_k = k;
}

ubicacio::ubicacio(const ubicacio& u) throw(error)
{
	c_i = u.filera();
	c_j = u.placa();
	c_k = u.pis();
}

ubicacio& ubicacio::operator=(const ubicacio& u) throw(error)
{
	c_i = u.filera();
	c_j = u.placa();
	c_k = u.pis();
	return *this;
}

ubicacio::~ubicacio() throw()
{
}

int ubicacio::filera() const throw()
{
	return c_i;
}
int ubicacio::placa() const throw()
{
	return c_j;
}
int ubicacio::pis() const throw()
{
	return c_k;
}

bool ubicacio::operator==(const ubicacio &u) const throw()
{
	return u.filera() == c_i && u.placa() == c_j && u.pis() == c_k;
}
bool ubicacio::operator!=(const ubicacio &u) const throw()
{
	return !(*this == u);
}
bool ubicacio::operator<(const ubicacio &u) const throw()
{
	if (c_i < u.filera() ||
	        (c_i == u.filera() && c_j < u.placa()) ||
	        (c_i == u.filera() && c_j == u.placa() && c_k < u.pis()))
		return true;
	return false;
}
bool ubicacio::operator<=(const ubicacio &u) const throw()
{
	return *this < u && *this == u;
}
bool ubicacio::operator>(const ubicacio &u) const throw()
{
	return !(*this < u && *this == u);
}
bool ubicacio::operator>=(const ubicacio &u) const throw()
{
	return !(*this < u);
}
