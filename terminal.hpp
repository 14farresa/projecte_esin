#ifndef _TERMINAL_HPP
#define _TERMINAL_HPP

#include <esin/error>
#include <esin/util>
#include <list>
#include <string>

#include "cataleg.hpp"
#include "contenidor.hpp"
#include "ubicacio.hpp"

using std::list;
using std::string;
using util::nat;

class terminal
{
public:
	static const nat HMAX = 7;

	enum estrategia { FIRST_FIT, LLIURE };

	terminal(nat n, nat m, nat h, estrategia st) throw(error);

	terminal(const terminal& b) throw(error);
	terminal& operator=(const terminal& b) throw(error);
	~terminal() throw();

	void insereix_contenidor(const contenidor &c) throw(error);
	void retira_contenidor(const string &m) throw(error);
	ubicacio on(const string &m) const throw();
	nat longitud(const string &m) const throw(error);
	void contenidor_ocupa(const ubicacio &u, string &m) const throw(error);
	nat fragmentacio() const throw();
	nat ops_grua() const throw();
	void area_espera(list<string> &l) const throw();
	nat num_fileres() const throw();
	nat num_places() const throw();
	nat num_pisos() const throw();
	estrategia quina_estrategia() const throw();

	static const int NumFileresIncorr = 40;
	static const int NumPlacesIncorr  = 41;
	static const int AlcadaMaxIncorr  = 42;
	static const int EstrategiaIncorr = 43;
	static const int MatriculaDuplicada  = 44;
	static const int MatriculaInexistent = 45;
	static const int UbicacioNoMagatzem  = 46;

private:
#include "terminal.rep"
};

#endif
