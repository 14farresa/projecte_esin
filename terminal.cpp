#include "cataleg.hpp"
#include "contenidor.hpp"
#include "terminal.hpp"
#include "ubicacio.hpp"

terminal::terminal(nat n, nat m, nat h, estrategia st) throw(error) : mu(10), mc(10)
{
	if (n < 1)
		throw error(NumFileresIncorr);
	if (m < 1)
		throw error(NumPlacesIncorr);
	if (m < 1)
		throw error(AlcadaMaxIncorr);
	if (m > HMAX)
		throw error(AlcadaMaxIncorr);
	if (st != terminal::FIRST_FIT && st != terminal::LLIURE)
		throw error(EstrategiaIncorr);

	fila = n;
	placa = m;
	alcada = h;
	est = st;
	op = 0;

	// Generem una taula tridimensional que contindrà, en cada posició, la
	// matrícula del contenidor que està ocupant-la
	taula = new std::string**[h];
	for (int i = 0; i < h; i++) {
		taula[i] = new std::string*[m];
		for (int j = 0; j < m; j++) {
			taula[i][j] = new std::string[h];
			for (int k = 0; k < n; k++) {
				taula[i][j][k] = "";
			}
		}
	}
}

terminal::terminal(const terminal& b) throw(error) : mu(10), mc(10)
{
	fila = b.num_fileres();
	placa = b.num_places();
	alcada = b.num_pisos();
	est = b.quina_estrategia();

	// Inicialitzem la taula
	taula = new std::string**[alcada];
	for (int i = 0; i < alcada; i++) {
		taula[i] = new std::string*[placa];
		for (int j = 0; j < placa; j++) {
			taula[i][j] = new std::string[fila];
			for (int k = 0; k < fila; k++) {
				taula[i][j][k] = "";
			}
		}
	}

	// Copiem els valors de la taula de la terminal passada per paràmetre
	for (int i = 0; i < fila; i++) {
		for (int j = 0; j < placa; j++) {
			for (int k = 0; k < alcada; k++) {
				taula[i][j][k] = b.taula[i][j][k];
			}
		}
	}
}

terminal& terminal::operator=(const terminal& b) throw(error)
{
   /* for (int i = 0; i < fila; i++) {*/
		//for (int j = 0; j < placa; j++) {
			//for (int k = 0; k < alcada; k++) {
				//delete taula[i][j][k];
			//}
		//}
	/*}*/
}

terminal::~terminal() throw()
{
}

void terminal::insereix_contenidor(const contenidor &c) throw(error)
{
	mc.assig(c.matricula(), c);

	if (est == terminal::FIRST_FIT) {
		bool inserit = false;
		for (int i = 0; i < fila && !inserit; i++) {
			for (int j = 0; j < placa; j++) {
				// Manté un comptador de la quantitat d'espais contigus buits
				// dins de cada fila per tal de calcular si hi ha espai
				// suficient per posar-hi el contenidor
				int count = 0;
				for (int k = 0; k < alcada; k++) {
					// Es trenca l'espai contigu; per tant, donem per segur que
					// no podem inserir-hi el conteindor, així que seguim
					if (taula[i][j][k] != "") {
						count = 0;
						continue;
					}
					// Segueix sent contigu
					count++;
					if (count == c.longitud() / 10)
						bool lliure = false;
						for (int z = 0; z < c.longitud() / 10; z++)
							// Comprovem que els espais de sobre són lliures, ja
							// que el fet que tinguem suficient espai lliure no
							// significa que podem inserir-ho, ja que ens podem
							// trobar contenidors a sobre que ens bloquegen
							//if (taula[i][j][k])
						for (int w = 0; w < c.longitud() / 10; w++) {
							// Canviem els valors de l'array desde la posició
							// actual cap endarrera
							taula[i][j][k - w] = c.matricula();
							inserit = true;
						}
				}
			}
		}
	} else {
	}
}

void terminal::retira_contenidor(const string &m) throw(error)
{
	if (!mc.existeix(m))
		throw error(MatriculaInexistent);

	int size = mc[m].longitud() / 10;
	for (int i = 0; i < fila; i++) {
		for (int j = 0; j < placa; j++) {
			// Comptador d'espais que tenen la matrícula donada. Quan arribem al
			// tamany de la matrícula, comprovem que a sobre no hi ha
			// contenidors. Si n'hi ha, haurem d'eliminar el contenidor de sobre
			// primer (crida recursiva); si no n'hi ha, el podem eliminar, i ho
			// fem cap en darrere
			int ct = 0;
			for (int k = 0; k < alcada; k++) {
				if (taula[i][j][k] != m) {
					ct = 0;
					continue;
				}
				if (++ct == size) {
					bool es_ultim = true;
					std::string matricula_superior;

					for (int w = 0; w < size && es_ultim; w++) {
						// Mirem els espais de sobre
						if (taula[i][j+1][k-w] != "") {
							es_ultim = false;
							matricula_superior = taula[i][j+1][k-w];
						}
					}
					if (!es_ultim)
						retira_contenidor(matricula_superior);
					else {
						// Eliminem el contenidor
						for (int m = 0; m < size; m++) {
							taula[i][j][k-m] = "";
						}
					}
				}
			}
		}
	}
}

ubicacio terminal::on(const string &m) const throw()
{
	//return mu[m];
}

nat terminal::longitud(const string &m) const throw(error)
{
	//for (std::list<contenidor> it = area_esp; it != area_esp.end(); it++)
	//{
	//if (*it.matricula() ==)
	//}
}

void terminal::contenidor_ocupa(const ubicacio &u, string &m) const throw(error)
{
	//m = taula[u.filera()][u.placa()][u.pis()];
}

nat terminal::fragmentacio() const throw()
{
	// Places lliures
	int ct;
	for (int i = 0; i < alcada; i++) {
		for (int j = 0; j < placa; j++) {
			for (int k = 0; k < fila; k++) {
				if (taula[i][j][k] == "")
					ct++;
			}
		}
	}
	return ct;
}

nat terminal::ops_grua() const throw()
{
	return op;
}

void terminal::area_espera(std::list<std::string> &l) const throw()
{
	l = area_esp;
	l.sort();
}

nat terminal::num_fileres() const throw()
{
	return fila;
}

nat terminal::num_places() const throw()
{
	return placa;
}

nat terminal::num_pisos() const throw()
{
	return alcada;
}

terminal::estrategia terminal::quina_estrategia() const throw()
{
	return est;
}
