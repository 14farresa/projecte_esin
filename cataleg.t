// FIXME: rehash
template <typename Valor>
cataleg<Valor>::cataleg(nat numelems) throw(error)
{
	max_elements = numelems;
	num_elements = 0;
	taula = new Node*[max_elements];
}

template <typename Valor>
cataleg<Valor>::cataleg(const cataleg<Valor>& c) throw(error)
{
	max_elements = c.num_elements;
	num_elements = 0;
	taula = new Node*[max_elements];

	for (int i = 0; i < c.quants(); i++)
	{
		if (c.taula[i] == NULL) continue;

		Node *tmp = c.taula[i];
		while (tmp != NULL)
		{
			assig(tmp->clau, tmp->valor);
			tmp = tmp->seg;
		}
	}
}

template <typename Valor>
cataleg<Valor>& cataleg<Valor>::operator=(const cataleg<Valor>& c) throw(error)
{
	// Eliminem els nodes de l'objecte actual
	for (int i = 0; i < max_elements; i++)
	{
		if (taula[i] == NULL) continue;

		Node *tmp = taula[i], *bk;
		while (tmp != NULL)
		{
			bk = tmp->seg;
			delete tmp;
			tmp = bk;
		}
	}

	taula = c.taula;
}

template <typename Valor>
cataleg<Valor>::~cataleg() throw()
{
	for (int i = 0; i < max_elements; i++)
	{
		if (taula[i] != NULL)
		{
			Node *tmp = taula[i], *bk;
			while (tmp != NULL)
			{
				bk = tmp->seg;
				delete tmp;
				tmp = bk;
			}
		}
	}
}

template <typename Valor>
void cataleg<Valor>::assig(const string &k, const Valor &v) throw(error)
{
	if (k.length() == 0)
		throw error(ClauStringBuit);

	int pos = genera_hash(k);
	Node *n = new Node;
	n->clau = k;
	n->valor = v;
	n->seg = NULL;

	if (taula[pos] == NULL)
		taula[pos] = n;
	else // Afegim l'element al final de la llista encadenada
	{
		Node *tmp = taula[pos], *ul;
		bool existeix = false;
		while (tmp != NULL && !existeix)
		{
			if (tmp->clau == k)
				existeix = true;
			else
			{
				if (tmp->seg == NULL)
					ul = tmp;
				tmp = tmp->seg;
			}
		}
		if (existeix)
			tmp->valor = v;
		else
		{
			ul->seg = n;
		}
	}
	num_elements++;
}

template <typename Valor>
void cataleg<Valor>::elimina(const string &k) throw(error)
{
	if (k.length() == 0)
		throw error(ClauStringBuit);

	int pos = genera_hash(k);
	if (taula[pos] == NULL)
		throw error(ClauInexistent);

	Node *tmp = taula[pos], *tmp2 = taula[pos];
	while (tmp != NULL)
	{
		if (tmp->clau == k)
		{
			// Primer de la llista
			if (tmp == taula[pos])
			{
				if (tmp->seg == NULL)
					taula[pos] = NULL;
				else
					taula[pos] = tmp->seg;
			}
			else
			{
				tmp2->seg = tmp->seg;
			}
			delete tmp;
			num_elements--;
		}
		tmp = tmp->seg;
		// tmp2 emmagatzema la direccio del node anterior; per tant, ha d'anar
		// per darrere en un node
		if (tmp != taula[pos])
			tmp2 = tmp2->seg;
	}
}

template <typename Valor>
bool cataleg<Valor>::existeix(const string &k) const throw()
{
	if (k.length() == 0)
		throw error(ClauStringBuit);

	int pos = genera_hash(k);
	if (taula[pos] == NULL)
		return false;

	Node *tmp = taula[pos];
	while (tmp != NULL)
	{
		if (tmp->clau == k)
			return true;
		tmp = tmp->seg;
	}
	return false;
}

template <typename Valor>
Valor cataleg<Valor>::operator[](const string &k) const throw(error)
{
	int pos = genera_hash(k);
	if (taula[pos] == NULL)
		throw error(ClauInexistent);

	Node *tmp = taula[pos];
	while (tmp != NULL)
	{
		if (tmp->clau == k)
			return tmp->valor;
		tmp = tmp->seg;
	}
	throw error(ClauInexistent);
}

template <typename Valor>
nat cataleg<Valor>::quants() const throw()
{
	return num_elements;
}
