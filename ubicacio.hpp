#ifndef _UBICACIO_HPP
#define _UBICACIO_HPP

#include <esin/error>
#include <esin/util>

class ubicacio
{
public:
	ubicacio(int i, int j, int k) throw(error);

	ubicacio(const ubicacio& u) throw(error);
	ubicacio& operator=(const ubicacio& u) throw(error);
	~ubicacio() throw();

	int filera() const throw();
	int placa() const throw();
	int pis() const throw();

	bool operator==(const ubicacio &u) const throw();
	bool operator!=(const ubicacio &u) const throw();
	bool operator<(const ubicacio &u) const throw();
	bool operator<=(const ubicacio &u) const throw();
	bool operator>(const ubicacio &u) const throw();
	bool operator>=(const ubicacio &u) const throw();

	static const int UbicacioIncorrecta = 10;

private:
#include "ubicacio.rep"
};

#endif
