#include <esin/error>
#include <esin/util>
#include <string>

#include "contenidor.hpp"

contenidor::contenidor(const string &m, nat l) throw(error)
{
	if (m.length() == 0)
		throw error(MatriculaIncorrecta);

	for (int i = 0; i < m.length(); i++)
	{
		// FIXME: Conte caracters el codi ascii del qual esta entre 65 i 90
		// inclos
		if (!((m[i] > 47 && m[i] < 58) || (m[i] > 64 && m[i] < 91)))
			throw error(MatriculaIncorrecta);
	}
	mat = m;

	if (l != 10 && l != 20 && l != 30)
		throw error(LongitudIncorrecta);
	lon = l;
}

contenidor::contenidor(const contenidor &u) throw(error)
{
	mat = u.matricula();
	lon = u.longitud();
}

contenidor& contenidor::operator=(const contenidor &u) throw(error)
{
	mat = u.mat;
	return *this;
}

contenidor::~contenidor() throw()
{
}

nat contenidor::longitud() const throw()
{
	return lon;
}
std::string contenidor::matricula() const throw()
{
	return mat;
}

bool contenidor::operator==(const contenidor &c) const throw()
{
	return c.longitud() == lon && c.matricula() == mat;
}
bool contenidor::operator!=(const contenidor &c) const throw()
{
	return !(*this == c);
}
bool contenidor::operator<(const contenidor &c) const throw()
{
	return mat < c.matricula() || (mat == c.matricula() && lon < c.longitud());
}

bool contenidor::operator<=(const contenidor &c) const throw()
{
	return !(*this > c);
}
bool contenidor::operator>(const contenidor &c) const throw()
{
	return mat > c.matricula() || (mat == c.matricula() && lon > c.longitud());
}
bool contenidor::operator>=(const contenidor &c) const throw()
{
	return !(*this < c);
}
