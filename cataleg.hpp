#ifndef _CATALEG_HPP
#define _CATALEG_HPP

#include <string>
#include <esin/error>
#include <esin/util>

using std::string;
using util::nat;

template <typename Valor>
class cataleg
{
public:
	explicit cataleg(nat numelems) throw(error);

	cataleg(const cataleg& c) throw(error);
	cataleg& operator=(const cataleg& c) throw(error);
	~cataleg() throw();

	void assig(const string &k, const Valor &v) throw(error);
	void elimina(const string &k) throw(error);
	bool existeix(const string &k) const throw();
	Valor operator[](const string &k) const throw(error);
	nat quants() const throw();

	static const int ClauStringBuit = 30;
	static const int ClauInexistent = 31;

private:
#include "cataleg.rep"
};

#include "cataleg.t"
#endif
