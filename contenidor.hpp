#ifndef _CONTENIDOR_HPP
#define _CONTENIDOR_HPP

#include <string>
#include <esin/error>
#include <esin/util>

using std::string;
using util::nat;

class contenidor
{
public:
	contenidor(const string &m, nat l) throw(error);

	contenidor(const contenidor &u) throw(error);
	contenidor& operator=(const contenidor &u) throw(error);
	~contenidor() throw();

	nat longitud() const throw();
	string matricula() const throw();

	bool operator==(const contenidor &c) const throw();
	bool operator!=(const contenidor &c) const throw();
	bool operator<(const contenidor &c) const throw();
	bool operator<=(const contenidor &c) const throw();
	bool operator>(const contenidor &c) const throw();
	bool operator>=(const contenidor &c) const throw();

	static const int MatriculaIncorrecta = 20;
	static const int LongitudIncorrecta = 21;

private:
#include "contenidor.rep"
};

#endif
